@echo off
setlocal enabledelayedexpansion



set VPROJECT=hl3
set game=%1
set map=%2



REM We should run a game under hl3 mod folder.
REM So if we're building nodegraphs, i.e., for hl2 maps, 
REM they will appear under hl3\maps\graphs.
REM We need to copy it to hl2\maps\graphs.
REM 
REM If we're building nodegraphs for hl3 - don't do anything.
REM 
REM TODO: Building nodegraphs for other mods is not supported at the moment.
REM 
REM Building cubemaps is essential, because after building cubemaps
REM nodegraph for the map becomes outdated.



if "!map!" neq "" (
	call :entry !map!
	goto quit
)

REM Little trickery-hackery to make skipping for-s work
goto mainloop



:entry
REM <game root dir>\hl2\maps\
set absolute_map_dir=%~dp1

REM <mapname>
set map_name=%~n1

REM <mapname>.bsp
set map_name_ext=%~nx1

REM hl2\maps\<mapname>.bsp
set relative_map_path=%1



echo Processing !map_name!... %1

REM 1-st pass: build cubemaps
echo 	... Building cubemaps
start /wait hl2.exe -game hl3 +map !map_name! -buildcubemaps
if !ERRORLEVEL! neq 0 echo ERROR building cubemaps&&goto :eof

REM 2-nd pass: build nodegraph
echo 	... Building nodegraph
start /wait hl2.exe -game hl3 +map !map_name! +exec nodebuilder +w1234
if !ERRORLEVEL! neq 0 echo ERROR building nodegraph&&goto :eof

if "!game!" neq "hl3" (
	echo 	... Copying compiled nodegraph to !game!
	
	if not exist !game!\maps\graphs\ mkdir !game!\maps\graphs\
	
	set ain_to_copy=hl3\maps\graphs\!map_name!.ain
	if not exist !ain_to_copy! echo ERROR .ain not found&&goto :eof
	move /Y hl3\maps\graphs\!map_name!.ain !game!\maps\graphs\!map_name!.ain
	if !ERRORLEVEL! neq 0 echo ERROR copying .ain-file&&goto :eof
)

goto :eof



:mainloop
for %%I in (!game!\maps\*.bsp) do (
	call :entry %%I
)

:quit
exit /B