@echo off

set VPROJECT=%1
set mapname=%2
set fast=%3
set rungame=%4

if "%VPROJECT%"=="" goto usage
if "%VPROJECT%"=="/?" goto usage
if "%mapname%"=="" goto usage


set extra_vvis=
set extra_vrad=
if not "%fast%"=="0" (
	if "%fast%"=="1" (
		set extra_vvis=-fast 
		set extra_vrad=-bounce 1 -extra -fast 
	) else if "%fast%"=="2" (
		set extra_vvis=-fast 
	) else if "%fast%"=="3" (
		set extra_vrad=-bounce 1 -extra -fast 
	)
)



bin\vbsp.exe -low %VPROJECT%\maps\%mapname%.vmf
bin\vvis.exe -low %extra_vvis%%VPROJECT%\maps\%mapname%.bsp
bin\vrad.exe -low %extra_vrad%%VPROJECT%\maps\%mapname%.bsp

if not "%rungame%"=="" (
	if "%rungame%"=="1" (
		hl2.exe -game %VPROJECT% +map %mapname%
	) else if "%rungame%"=="2" (
		hl2.exe -game %VPROJECT% -buildcubemaps +map %mapname%
	)
)
goto quit

:usage
echo.
echo Usage: compile_bsp.bat VPROJECT map [fast=0] [rungame=0] [buildcubemaps=0]
echo.
echo fast: 0 - normal (default), 1 - fast vvis/vrad, 2 - fast vvis, 3 - fast vrad
echo rungame: 0 - don't run game (default), 1 - run game after compile, 2 - only build cubemaps
goto quit

:quit
exit /B
