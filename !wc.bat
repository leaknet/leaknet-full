@echo off
setlocal enabledelayedexpansion

set project=%*


REM if "%project:~-1%" == "\" set "MYDIR1=%project:~0,-1%"
REM for %%f in ("%MYDIR1%") do set "moddir=%%~nxf"

set moddir=
for %%f in ("!project!") do set moddir=%%~nxf

if "!moddir!"=="" (
	echo.
	echo Usage: Drag mod folder onto this .bat-file.
	echo.
	echo %~n0
	pause
	goto quit
)

echo !moddir!

set VPROJECT=!moddir!
set program=%~n0
start bin\!program!.exe

:quit
exit /B